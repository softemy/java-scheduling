package Core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;


public class Schedule
{
	// Number of working hours per day
	public static final int DAY_HOURS	= 12;
	// Number of days in week
	public static final int DAYS_NUM = 5;
	
	// Number of crossover points of parent's class tables
	private int numberOfCrossoverPoints;

	// Number of classes that is moved randomly by single mutation operation
	private int mutationSize;

	// Probability that crossover will occure
	private int crossoverProbability;

	// Probability that mutation will occure
	private int mutationProbability;

	// Fitness value of chromosome
	private float fitness;

	// Flags of class requiroments satisfaction
	private Vector<Boolean> criteria = new Vector<Boolean>();

	// Time-space slots, one entry represent one hour in one classroom
	private Vector<List<CourseClass>> slots = new Vector<List<CourseClass>>();

	// Class table for chromosome
	// Used to determine first time-space slot used by class
	private HashMap<CourseClass, Integer> classes = new HashMap<CourseClass, Integer>();
	
	private Random r = new Random();
	
	// Initializes chromosomes with configuration block (setup of chromosome)
    public Schedule(int numberOfCrossoverPoints, int mutationSize,
				   int crossoverProbability, int mutationProbability) 
    {
	    this.mutationSize = mutationSize;
	    this.numberOfCrossoverPoints = numberOfCrossoverPoints;
	    this.crossoverProbability = crossoverProbability;
	    this.mutationProbability = mutationProbability;
	    this.fitness = 0;
	 
	    // reserve space for time-space slots in chromosomes code
	    this.slots.setSize(DAYS_NUM * DAY_HOURS * Configuration.GetInstance().GetNumberOfRooms());
        for(int i = 0; i < slots.size(); i++)
        {
          this.slots.set(i, null);	
        }
        
	    // reserve space for flags of class requirements
        this.criteria.setSize(Configuration.GetInstance().GetNumberOfCourseClasses() * 5);
	    for(int i = 0; i < criteria.size(); i++)
        {
	    	this.criteria.set(i, false);	
        }
    }

    // Copy constructor
    public Schedule(Schedule c, boolean setupOnly)
    {
	    if( !setupOnly )
	    {
		    // copy code
		    this.slots = c.slots;
		    this.classes = c.classes;

		    // copy flags of class requirements
		    this.criteria = c.criteria;

		    // copy fitness
		    this.fitness = c.fitness;
	    }
	    else
	    {
		    // reserve space for time-space slots in chromosomes code
	    	this.slots.setSize(DAYS_NUM * DAY_HOURS * Configuration.GetInstance().GetNumberOfRooms() ); 
		    
		    // reserve space for flags of class requirements
	    	this.criteria.setSize( Configuration.GetInstance().GetNumberOfCourseClasses() * 5 );
	    }

	    // copy parameters
	    this.numberOfCrossoverPoints = c.numberOfCrossoverPoints;
	    this.mutationSize = c.mutationSize;
	    this.crossoverProbability = c.crossoverProbability;
	    this.mutationProbability = c.mutationProbability;
    }

    // Makes copy ot chromosome
    public Schedule MakeCopy(boolean setupOnly)
    {
	    // make object by calling copy constructor and return smart pointer to new object
	    return new Schedule(this, setupOnly );
    }

    // Makes new chromosome with same setup but with randomly chosen code
    Schedule MakeNewFromPrototype()
    {
	    // number of time-space slots
	    int size = slots.size();

	    // make new chromosome, copy chromosome setup
	    Schedule newChromosome = new Schedule( this, true );

	    // place classes at random position
	    List<CourseClass> c = Configuration.GetInstance().GetCourseClasses();
	    
	    for(Iterator<CourseClass> it = c.iterator(); it.hasNext(); )
	    {
	    	CourseClass cls = it.next();
		    // determine random position of class
		    int nr = Configuration.GetInstance().GetNumberOfRooms();
		    int dur = cls.GetDuration();
		    int day = (r.nextInt() % DAYS_NUM + DAYS_NUM) % DAYS_NUM;
		    int room = (r.nextInt() % nr + nr) % nr;
		    int time = (r.nextInt() % ( DAY_HOURS + 1 - dur ) + DAY_HOURS + 1 - dur) % (DAY_HOURS + 1 - dur);
		    int pos = day * nr * DAY_HOURS + room * DAY_HOURS + time;

		    // fill time-space slots, for each hour of class
		    for(int i = dur - 1; i >= 0; i--)
		    {
		    	List<CourseClass> clsList = newChromosome.slots.get(pos + i);
		    	if(clsList == null)
		    	{
		    		clsList = new LinkedList<CourseClass>();
		    		newChromosome.slots.set(pos + i, clsList);
		    	}
		    	clsList.add(cls);
		    }

		    // insert in class table of chromosome
		    newChromosome.classes.put(cls, pos);
	     }

	     newChromosome.CalculateFitness();

	     // return smart pointer
	     return newChromosome;
    }

    // Performes crossover operation using to chromosomes and returns pointer to offspring
    Schedule Crossover(Schedule parent2)
    {
	    // check probability of crossover operation
	    if(r.nextInt() % 100 > crossoverProbability )
		    // no crossover, just copy first parent
		    return new Schedule(this, false);

	    // new chromosome object, copy chromosome setup
	    Schedule n = new Schedule(this, true);

	    // number of classes
	    int size = classes.size();
	    
	    Vector<Boolean> cp = new Vector<Boolean>(size);
	    cp.setSize(size);
        for(int i = 0; i < size; i++)
        {
        	cp.set(i, false);
        }
	    
	    // determine crossover point (randomly)
	    for(int i = numberOfCrossoverPoints; i > 0; i--)
	    {
		    while(true)
		    {
			    int p = (r.nextInt() % size + size) % size;
			    
			    if(!cp.get(p))
			    {
				    cp.set(p, true);
				    break;
			    }
		    }
	    }

        Iterator<Map.Entry<CourseClass, Integer>> it1 = classes.entrySet().iterator();
        Iterator<Map.Entry<CourseClass, Integer>> it2 = parent2.classes.entrySet().iterator();
	    
	    // make new code by combining parent codes
	    boolean first = r.nextInt() % 2 == 0;
	    for( int i = 0; i < size; i++ )
	    {
	    	Map.Entry<CourseClass, Integer> kvp1 = it1.next();
	    	Map.Entry<CourseClass, Integer> kvp2 = it2.next();
	    	
		    if( first )
		    {
			    // insert class from first parent into new chromosome's calss table
			    n.classes.put(kvp1.getKey(), kvp1.getValue());
			    // all time-space slots of class are copied
			    for(int ii = kvp1.getKey().GetDuration() - 1; ii >= 0; ii--)
			    {
			    	List<CourseClass> clsList = n.slots.get(kvp1.getValue() + ii);
			    	if(clsList == null)
			    	{
			    		n.slots.set(kvp1.getValue() + ii, new LinkedList<CourseClass>());
			    	}
				    n.slots.get(kvp1.getValue() + ii).add(kvp1.getKey());
			    }
		    }
		    else
		    {
			    // insert class from second parent into new chromosome's calss table
			    n.classes.put(kvp2.getKey(), kvp2.getValue());
			    // all time-space slots of class are copied
			    for(int ii = kvp2.getKey().GetDuration() - 1; ii >= 0; ii--)
			    {
			    	List<CourseClass> clsList = n.slots.get(kvp2.getValue() + ii);
			    	if(clsList == null)
			    	{
			    		n.slots.set(kvp2.getValue() + ii, new LinkedList<CourseClass>());
			    	}
				    n.slots.get(kvp2.getValue() + ii).add(kvp2.getKey());
			    }
		    }

		    // crossover point
		    if(cp.get(i))
			// change soruce chromosome
			    first = !first;
	    }

	    n.CalculateFitness();

	    // return smart pointer to offspring
	    return n;
    }

    // Performs mutation on chromosome
    void Mutation()
    {
	    // check probability of mutation operation
	    if(r.nextInt() % 100 > mutationProbability)
		    return;

	    // number of classes
	    int numberOfClasses = classes.size();
	    // number of time-space slots
	    int size = slots.size();

	    // move selected number of classes at random position
	    for(int i = mutationSize; i > 0; i--)
	    {
		    // select ranom chromosome for movement
		    int mpos = r.nextInt() % numberOfClasses;
		    int pos1 = 0;
		    
		    Iterator<Map.Entry<CourseClass, Integer>> it = classes.entrySet().iterator();
		    Map.Entry<CourseClass, Integer> kvp = it.next();
		    for(; mpos > 0; kvp = it.next(), mpos--)
			    ;

		    // current time-space slot used by class
		    pos1 = kvp.getValue();

		    CourseClass cc1 = kvp.getKey();

		    // determine position of class randomly
		    int nr = Configuration.GetInstance().GetNumberOfRooms();
		    int dur = cc1.GetDuration();
		    int day = (r.nextInt() % DAYS_NUM + DAYS_NUM) % DAYS_NUM;
		    int room = (r.nextInt() % nr + nr) % nr;
		    int time = (r.nextInt() % ( DAY_HOURS + 1 - dur ) + DAY_HOURS + 1 - dur) %(DAY_HOURS + 1 - dur);
		    int pos2 = day * nr * DAY_HOURS + room * DAY_HOURS + time;

		    // move all time-space slots
		    for(int ii = dur - 1; ii >= 0; ii--)
		    {
			    // remove class hour from current time-space slot
			    List<CourseClass> cl = slots.get(pos1 + ii);
			    for(int j = 0; j < cl.size(); j++)
			    {
				    if(cl.get(j).equals(cc1))
				    {
					    cl.remove(j);
					    break;
				    }
			    }

			    // move class hour to new time-space slot
			    if(slots.get(pos2 + ii) == null)
			    {
			    	slots.set(pos2 + ii, new LinkedList<CourseClass>());
			    }
			    slots.get(pos2 + ii).add(cc1);
		    }

		    // change entry of class table to point to new time-space slots
		    classes.put(cc1, pos2);
	    }

	    CalculateFitness();
    }

    // Calculates fitness value of chromosome
    void CalculateFitness()
    {
    	fitness = GetFitnessAll();
    	System.out.println("Fitness: " + fitness);
    }
    
    // Calculates fitness value of chromosome
    float GetFitnessAll()
    {
	    // chromosome's score
	    int score = 0;

	    int numberOfRooms = Configuration.GetInstance().GetNumberOfRooms();
	    int daySize = DAY_HOURS * numberOfRooms;

	    int ci = 0;

	    // check criterias and calculate scores for each class in schedule
	    for(Iterator<Map.Entry<CourseClass, Integer>> it = classes.entrySet().iterator(); it.hasNext(); ci += 5)
	    {
		    // coordinate of time-space slot
	    	Map.Entry<CourseClass, Integer> kvp = it.next();
	    	
		    int p = kvp.getValue();
		    int day = p / daySize;
		    int time = p % daySize;
		    int room = time / DAY_HOURS;
		    time = time % DAY_HOURS;

		    int dur = kvp.getKey().GetDuration();

		    // check for room overlapping of classes
		    boolean ro = false;
		    for(int i = dur - 1; i >= 0; i--)
		    {
			    if(slots.get(p + i).size() > 1 )
			    {
				    ro = true;
				    break;
			    }
		    }

		    // on room overlapping
		    if(!ro)
		    {
		    	System.out.println("false ro");
			    score++;
		    }

		    criteria.set(ci+0, !ro);

		    CourseClass cc = kvp.getKey();
		    Room r = Configuration.GetInstance().GetRoomById(room);
		    // does current room have enough seats
		    criteria.set(ci + 1, r.GetNumberOfSeats() >= cc.GetNumberOfSeats());
		    if(criteria.get(ci + 1))
		    {
		    	System.out.println("Enough seats");
			    score++;
		    }

		    // does current room have computers if they are required
		    criteria.set(ci+2, !cc.IsLabRequired() || (cc.IsLabRequired() && r.IsLab()));
		    if(criteria.get(ci+2))
		    {
		    	System.out.println("Lab info matches");
		    	score++;
		    }

		    boolean po = false, go = false;
		    boolean isTotalOverlap = false;
		    // check overlapping of classes for lecturers and student groups
		    for(int i = numberOfRooms, t = day * daySize + time; i > 0 && !isTotalOverlap; i--, t += DAY_HOURS)
		    {
			    // for each hour of class
			    for( int j = dur - 1; j >= 0 && !isTotalOverlap; j-- )
			    {
				    // check for overlapping with other classes at same time
				   
				    if(slots.get(j+i) == null)
				    {
				    	slots.set(j+i, new LinkedList<CourseClass>());
				    }
				    List<CourseClass> cl = slots.get(j+i);
				    
				    for(Iterator<CourseClass> it2 = cl.iterator(); it2.hasNext() && !isTotalOverlap;)
				    {
				    	CourseClass cls = it2.next();
					    if(cc != cls)
					    {
						    // lecturer overlaps?
						    if( !po && cc.LecturerOverlaps(cls) )
							    po = true;

						    // student group overlaps?
						    if( !go && cc.GroupsOverlap(cls) )
							    go = true;

						    // both type of overlapping? no need to check more
						    if( po && go )
						    {
						    	isTotalOverlap = true;
						    }
					    }
				    }
			    }
		    }

		    // lecturers have no overlapping classes?
		    if(!po)
			  score++;
		    criteria.set(ci+3, !po);

		    // student groups has no overlapping classes?
		    if(!go)
			  score++;
		    criteria.set(ci+4, !go);
	    }

	    System.out.println("Score: " + score + ",divisor:" + Configuration.GetInstance().GetNumberOfCourseClasses() * DAYS_NUM);
	    // calculate fitness value based on score
	    return (float)score / (Configuration.GetInstance().GetNumberOfCourseClasses() * DAYS_NUM);
    }
    
    // Returns fitness value of chromosome
	public float GetFitness() { return fitness; }

	// Returns reference to table of classes
	public HashMap<CourseClass, Integer> GetClasses() { return classes; }

	// Returns array of flags of class requiroments satisfaction
	public Vector<Boolean> GetCriteria() { return criteria; }

	// Return reference to array of time-space slots
	public Vector<List<CourseClass>> GetSlots() { return slots; }
}
