package Core;
// Stores data about classroom
public class Room
{
	// ID counter used to assign IDs automatically
	private static int nextRoomId;

	// Room ID - automatically assigned
	private int id;

	// Room name
	private String name;

	// Inidicates if room has computers
	private boolean lab;

	// Number of seats in room
	private int numberOfSeats;

	// Initializes room data and assign ID to room
	public Room(String name, boolean lab, int numberOfSeats) 
    { 
		this.id = nextRoomId++;
		this.name = name;
		this.lab = lab;
		this.numberOfSeats = numberOfSeats; 
    }

	// Returns room ID
	public int GetId() { return id; }

	// Returns name
	public String GetName() { return name; }

	// Returns TRUE if room has computers otherwise it returns FALSE
	public boolean IsLab() { return lab; }

	// Returns number of seats in room
	public int GetNumberOfSeats() { return numberOfSeats; }

	// Restarts ID assigments
	public static void RestartIDs() { nextRoomId = 0; }
	
	public String toString() {
		return name;
	}

};