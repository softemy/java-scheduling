package Core;

import java.util.LinkedList;
import java.util.List;


public class Lecturer {
	private int id;

	// Lecturer's ID
	private String name;

	// List of classes that professor teaches
	private List<CourseClass> courseClasses = new LinkedList<CourseClass>();
	
	// Initializes professor data
	public Lecturer(int id, String name) 
	{
		this.id = id;
		this.name = name;
	}

	// Bind professor to course
	public void AddCourseClass(CourseClass courseClass)
	{
		this.courseClasses.add( courseClass );
	}
	
	// Returns professor's ID
	public int GetId() { return id; }

	// Returns professor's name
	public String GetName() { return name; }

	// Returns reference to list of classes that professor teaches
	public List<CourseClass> GetCourseClasses() { return courseClasses; }

	// Compares ID's of two objects which represent professors
	public boolean equals(Object other) 
	{ 
		if(other == null) return false;
		if(!(other instanceof Lecturer)) return false;
		
		Lecturer rhs = (Lecturer)other;
		return id == rhs.id; 
	}
}
