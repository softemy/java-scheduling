package Core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Configuration
{
    // Global instance
    private static Configuration instance = new Configuration();

	// Returns reference to global instance
	public static Configuration GetInstance() { return instance; }

	// Parsed lecturers
    private	HashMap<Integer, Lecturer> lecturers = new HashMap<Integer, Lecturer>();

	// Parsed student groups
    private HashMap<Integer, StudentsGroup> studentGroups = new HashMap<Integer, StudentsGroup>();

	// Parsed courses
    private HashMap<Integer, Course> courses = new HashMap<Integer, Course>();

	// Parsed rooms
    private HashMap<Integer, Room> rooms = new HashMap<Integer, Room>();

	// Parsed classes
    private List<CourseClass> courseClasses = new LinkedList<CourseClass>();

	// Inidicate that configuration is not prsed yet
	private boolean isEmpty;


	// Initialize data
	private Configuration() 
	{
	  this.isEmpty = true; 	
	}

	// Returns pointer to lecturer with specified ID
	// If there is no lecturer with such ID method returns NULL
	public Lecturer GetLecturerById(int id)
	{ 
		return lecturers.containsKey(id) ? lecturers.get(id) : null;
	}

	// Returns number of parsed lecturers
	public int GetNumberOfLecturers() { return lecturers.size(); }

	// Returns pointer to student group with specified ID
	// If there is no student group with such ID method returns NULL
	public StudentsGroup GetStudentsGroupById(int id)
	{ 
		return studentGroups.containsKey(id) ? studentGroups.get(id) : null;
	}

	// Returns number of parsed student groups
	public int GetNumberOfStudentGroups() { return studentGroups.size(); }

	// Returns pointer to course with specified ID
	// If there is no course with such ID method returns NULL
	public Course GetCourseById(int id)
	{ 
		return courses.containsKey(id) ? courses.get(id) : null;
	}

	public int GetNumberOfCourses() { return courses.size(); }

	// Returns pointer to room with specified ID
	// If there is no room with such ID method returns NULL
	public Room GetRoomById(int id)
	{ 
		return rooms.containsKey(id) ? rooms.get(id) : null;
	}
	
	public HashMap<Integer, Room> GetRooms() {
		return rooms;
	}

	// Returns number of parsed rooms
	public int GetNumberOfRooms() { return rooms.size(); }

	// Returns reference to list of parsed classes
	public List<CourseClass> GetCourseClasses() { return courseClasses; }

	// Returns number of parsed classes
	public int GetNumberOfCourseClasses() { return courseClasses.size(); }

	// Returns TRUE if configuration is not parsed yet
	public boolean IsEmpty() { return isEmpty; }
	
	
	// Parse file and store parsed object
	public void ParseFile(String fileName) throws Exception
	{
		// clear previously parsed objects
		lecturers.clear();
		studentGroups.clear();
		courses.clear();
		rooms.clear();
		courseClasses.clear();

		Room.RestartIDs();

		BufferedReader br = null;
		try
		{
		// open file
		br = new BufferedReader(new FileReader(fileName));

		String line;
		while((line = br.readLine()) != null)
		{
			// get lines until start of new object is not found
			line = line.trim();

			// get type of object, parse obect and store it
			if(line.compareTo("#instructor") == 0)
			{
				Lecturer p = ParseLecturer(br);
				if(p != null)
					lecturers.put(p.GetId(), p);
			}
			else if( line.compareTo("#group") == 0 )
			{
				StudentsGroup g = ParseStudentsGroup(br);
				if(g != null)
					studentGroups.put(g.GetId(), g);
			}
			else if( line.compareTo("#course") == 0 )
			{
				Course c = ParseCourse(br);
				if(c != null)
					courses.put(c.GetId(), c);
			}
			else if( line.compareTo("#room") == 0 )
			{
				Room r = ParseRoom(br);
				if(r != null)
					rooms.put(r.GetId(), r);
			}
			else if( line.compareTo("#class") == 0 )
			{
				CourseClass c = ParseCourseClass(br);
				if(c != null)
					courseClasses.add(c);
			}
		}
		}
		catch(Exception ex)
		{
		  if(br != null)
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			throw ex;
		}
		
		isEmpty = false;
	}

	// Reads lecturer's data from config file, makes object and returns pointer to it
	// Returns NULL if method cannot parse configuration data
	private Lecturer ParseLecturer(BufferedReader br) throws IOException
	{
		int id = 0;
		String name = null;

		while(true)
		{
		  StringBuilder key = new StringBuilder();
		  StringBuilder value = new StringBuilder();
		  
		  // get key - value pair
		  if(!GetConfigBlockLine(br, key, value))
			break;

		  // get value of key
		  if(key.toString().compareTo("id") == 0)
		  {
			  id = Integer.parseInt(value.toString());
		  }
		  else if(key.toString().compareTo("name") == 0)
		  {
			  name = value.toString();
		  }
		}

		// make object and return pointer to it
		return id == 0 ? null : new Lecturer( id, name );
	}

	// Reads lecturer's data from config file, makes object and returns pointer to it
	// Returns NULL if method cannot parse configuration data
	private StudentsGroup ParseStudentsGroup(BufferedReader br) throws IOException
	{
		int id = 0, number = 0;
		String name = null;

		while(true)
		{
	      StringBuilder key = new StringBuilder();
		  StringBuilder value = new StringBuilder();
	
		  // get key - value pair
		  if( !GetConfigBlockLine(br, key, value ) )
			break;

		  // get value of key
		  if( key.toString().compareTo("id") == 0 )
			id = Integer.parseInt(value.toString());
		  else if(key.toString().compareTo("name") == 0 )
			name = value.toString();
		  else if(key.toString().compareTo("size") == 0 )
			number = Integer.parseInt(value.toString());
		}
		
		// make object and return pointer to it
		return id == 0 ? null : new StudentsGroup( id, name, number );
	}

	// Reads course's data from config file, makes object and returns pointer to it
	// Returns NULL if method cannot parse configuration data
	private Course ParseCourse(BufferedReader br) throws IOException
	{
		int id = 0;
		String name = null;

		while(true)
		{
		  StringBuilder key = new StringBuilder();
		  StringBuilder value = new StringBuilder();
			
		  // get key - value pair
		  if(!GetConfigBlockLine(br, key, value ))
			break;

		  // get value of key
		  if(key.toString().compareTo("id") == 0)
			id = Integer.parseInt(value.toString());
		  else if(key.toString().compareTo("name") == 0)
			name = value.toString();
		}

		// make object and return pointer to it
		return id == 0 ? null : new Course(id, name);
	}

	// Reads rooms's data from config file, makes object and returns pointer to it
	// Returns NULL if method cannot parse configuration data
	private Room ParseRoom(BufferedReader br) throws IOException
	{
		int number = 0;
		boolean lab = false;
		String name = null;

		while(true)
		{
		  StringBuilder key = new StringBuilder();
		  StringBuilder value = new StringBuilder();

		  // get key - value pair
		  if(!GetConfigBlockLine(br, key, value))
		    break;

		  // get value of key
		  if(key.toString().compareTo("name") == 0)
			name = value.toString();
		  else if(key.toString().compareTo("lab") == 0)
			lab = (value.toString().compareTo( "true" ) == 0);
		  else if(key.toString().compareTo("size") == 0)
			number = Integer.parseInt(value.toString());
		}
		
		// make object and return pointer to it
		return number == 0 ? null : new Room(name, lab, number );
	}

	// Reads class' data from config file, makes object and returns pointer to it
	// Returns NULL if method cannot parse configuration data
	private CourseClass ParseCourseClass(BufferedReader br) throws IOException
	{
		int pid = 0, cid = 0, dur = 1;
		boolean lab = false;

		List<StudentsGroup> groups = new LinkedList<StudentsGroup>();

		while(true)
		{
			StringBuilder key = new StringBuilder();
			StringBuilder value = new StringBuilder();

		  // get key - value pair
		  if( !GetConfigBlockLine(br, key, value))
			break;

		  // get value of key
		  if(key.toString().compareTo("instructor") == 0 )
			pid = Integer.parseInt(value.toString());
		  else if(key.toString().compareTo("course") == 0)
			cid = Integer.parseInt(value.toString());
		  else if(key.toString().compareTo("lab") == 0)
			lab = (value.toString().compareTo( "true" ) == 0);
		  else if(key.toString().compareTo("duration") == 0)
			dur = Integer.parseInt(value.toString());
		  else if(key.toString().compareTo("group") == 0)
		  {
			StudentsGroup g = GetStudentsGroupById(Integer.parseInt(value.toString()));
			if(g != null)
				groups.add(g);
		  }
		}

		// get lecturer who teaches class and course to which this class belongs
		Lecturer p = GetLecturerById( pid );
		Course c = GetCourseById( cid );

		// does lecturer and class exists
		if(c == null || p == null)
			return null;

		// make object and return pointer to it
		CourseClass cc = new CourseClass( p, c, groups, lab, dur );
		return cc;
	}

	// Reads one line (key - value pair) from configuration file
	private boolean GetConfigBlockLine(BufferedReader br, StringBuilder key, StringBuilder value) throws IOException
	{
		String line;

		// end of file
		while((line = br.readLine()) != null)
		{
			// read line from config file
			line = line.trim();

			// end of object's data 
			if(line.compareTo( "#end" ) == 0)
				return false;

			int p = line.indexOf('=');
			if(p >= 0)
			{	// key
				String _key = line.substring(0, p);
			    _key = _key.trim();
                key.append(_key);
                
				// value
				String _value = line.substring(p + 1);
				_value = _value.trim();
				value.append(_value);

				// key - value pair read successfully
				return true;
			}
		}

		// error
		return false;
	}

}