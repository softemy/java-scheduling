package Core;

import GUI.Gui;

public class ScheduleObserver
{
  // Event that blocks caller until algorithm finishes execution 
  private Object event;

  // Window which displays schedule
  private Gui window;

  // Called when algorithm starts execution
  private void BlockEvent() 
  { 
	 /* try {
		event.wait();
	} catch (InterruptedException e) {
		e.printStackTrace();
	} */
  }

  // Called when algorithm finishes execution
  private void ReleaseEvent() 
  { 
	  /*event.notify(); */
  }

  // Initializes observer
  public ScheduleObserver()
  { 
	  window = null; 
	  event = new Object(); 
  }

  
  // Block caller's thread until algorithm finishes execution
  public void WaitEvent() 
  { 
	  try {
		event.wait();
	  } 
	  catch (InterruptedException e) {
		e.printStackTrace();
	  }
  }
		
  // Sets window which displays schedule
  public void SetWindow(Gui gui) 
  { 
	  this.window = gui; 
  }

  // Handles event that is raised when algorithm finds new best chromosome
  public void NewBestChromosome(Schedule newChromosome)
  {
	if(window != null)
		window.SetSchedule(newChromosome);
  }

  // Handles event that is raised when state of execution of algorithm is changed
  public void EvolutionStateChanged(Algorithm.AlgorithmState newState)
  {
	if(window != null)
		window.SetNewState(newState);

	if(newState != Algorithm.AlgorithmState.AS_RUNNING)
		ReleaseEvent();
	else BlockEvent();
  }
}
