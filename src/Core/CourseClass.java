package Core;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

// Stores data about single class
public class CourseClass
{
	// Profesor who teaches
    private	Lecturer lecturer;

	// Course to which class belongs
	private Course course;

	// Student groups who attend class
	private List<StudentsGroup> groups = new LinkedList<StudentsGroup>();

	// Number of seats (students) required in room
	private int numberOfSeats;

	// Initicates wheather class requires computers
	private boolean requiresLab;

	// Duration of class in hours
	private int duration;
	

	// Initializes class object
	public CourseClass(Lecturer lecturer, Course course, List<StudentsGroup> groups,
							 boolean requiresLab, int duration) 
							 
	{
		 this.lecturer = lecturer;
		 this.course = course;
		 this.numberOfSeats = 0;
		 this.requiresLab = requiresLab;
		 this.duration = duration;
		
		// bind lecturer to class
		 this.lecturer.AddCourseClass( this );

		// bind student groups to class
		for(Iterator<StudentsGroup> it = groups.iterator(); it.hasNext();)
		{
			StudentsGroup sgp = it.next();
			sgp.AddClass(this);
			this.groups.add(sgp);
			this.numberOfSeats += sgp.GetNumberOfStudents();
		}
	}

	// Returns TRUE if another class has one or overlapping student groups.
	public boolean GroupsOverlap(CourseClass c )
	{
		for(Iterator<StudentsGroup> it1 = groups.iterator(); it1.hasNext();)
		{
			StudentsGroup sgp1= it1.next();
			for(Iterator<StudentsGroup> it2 = c.groups.iterator(); it2.hasNext();)
			{
				StudentsGroup sgp2= it2.next();
				if(sgp1.equals(sgp2))
					return true;
			}
		}

		return false;
	}
	
	// Returns TRUE if another class has same lecturer.
	public boolean LecturerOverlaps(CourseClass c ) { return lecturer == c.lecturer; }

	// Return pointer to lecturer who teaches
	public Lecturer GetLecturer() { return lecturer; }

	// Return pointer to course to which class belongs
	public Course GetCourse() { return course; }

	// Returns reference to list of student groups who attend class
	public List<StudentsGroup> GetGroups() { return groups; }

	// Returns number of seats (students) required in room
	public int GetNumberOfSeats() { return numberOfSeats; }

	// Returns TRUE if class requires computers in room.
	public boolean IsLabRequired() { return requiresLab; }

	// Returns duration of class in hours
	public int GetDuration() { return duration; }
};