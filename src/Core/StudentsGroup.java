package Core;

import java.util.LinkedList;
import java.util.List;

public class StudentsGroup
{
	// Student group ID
	private int _id;

	// Name of student group
	private String _name;

	// Number of students in group
	private int _numberOfStudents;

	// List of classes that group attends
	private List<CourseClass> _courseClasses = new LinkedList<CourseClass>();


	// Initializes student group data
	public StudentsGroup(int id, String name, int numberOfStudents)
	{
		_id = id;
		_name = name;
		_numberOfStudents = numberOfStudents;
	}

	// Bind group to class
	public void AddClass(CourseClass courseClass)
	{
		_courseClasses.add(courseClass);
	}

	// Returns student group ID
	public int GetId() { return _id; }

	// Returns name of student group
	public String GetName() { return _name; }

	// Returns number of students in group
	public int GetNumberOfStudents() { return _numberOfStudents; }

	// Returns reference to list of classes that group attends
	public List<CourseClass> GetCourseClasses() { return _courseClasses; }

	// Compares ID's of two objects which represent student groups
	public boolean equals(Object other) 
	{ 
		if(other == null) return false;
		if(!(other instanceof StudentsGroup)) return false;
		
		StudentsGroup rhs = (StudentsGroup)other;
		return _id == rhs._id; 
	}
}