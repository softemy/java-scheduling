package Core;

public class Course 
{
	// Course ID
	private int _id;

	// Course name
	private String _name;

		// Initializes course
	public 	Course(int id, String name)
	{
		_id = id;
		_name = name;
	}

	// Returns course ID
	public int GetId() { return _id; }

	// Returns course name
	public String GetName() { return _name; }
}
