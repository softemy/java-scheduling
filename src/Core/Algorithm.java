package Core;

import java.util.Random;
import java.util.Vector;

public class Algorithm 
{
	public enum AlgorithmState
	{
		AS_USER_STOPED,
		AS_CRITERIA_STOPPED,
		AS_RUNNING
	};
	
	// Population of chromosomes
	private Vector<Schedule> chromosomes = new Vector<Schedule>();

	// Inidicates wheahter chromosome belongs to best chromosome group
	private Vector<Boolean> bestFlags = new Vector<Boolean>();

	// Indices of best chromosomes
	private Vector<Integer> bestChromosomes = new Vector<Integer>();

	// Number of best chromosomes currently saved in best chromosome group
	private int currentBestSize;

	// Number of chromosomes which are replaced in each generation by offspring
	private int replaceByGeneration;

	// Pointer to algorithm observer
	private ScheduleObserver observer;

	// Prototype of chromosomes in population
	private Schedule prototype;

	// Current generation
	int currentGeneration;

	// State of execution of algorithm
	AlgorithmState state;
	
	boolean useMaxGen = false;
	int maxGenNum = 0;
	
	public void setMaxGeneration(int maxGenNum)
	{
	  this.maxGenNum = maxGenNum;
	  useMaxGen = true;
	}
	
	private static class AlgorithmHolder
	{
		// make new global instance of algorithm using chromosome prototype
		private static Algorithm instance =  new Algorithm( 100, 8, 5, new Schedule( 2, 2, 80, 3 ), new ScheduleObserver() );
	}
		
	// Returns reference to global instance of algorithm
	public static Algorithm GetInstance()
	{
		return AlgorithmHolder.instance;
	}

	// Frees memory used by global instance
	void FreeInstance()
	{
		// free memory used by global instance if it exists
		if( AlgorithmHolder.instance != null)
		{
			 AlgorithmHolder.instance = null;
		}
	}

	// Initializes genetic algorithm
	private Algorithm(int numberOfChromosomes, int replaceByGeneration, int trackBest,
						 Schedule prototype, ScheduleObserver observer)
	{
		 this.replaceByGeneration = replaceByGeneration;
		 this.currentBestSize = 0;
		 this.prototype = prototype;
		 this.observer = observer;
		 this.currentGeneration = 0;
		 this.state = AlgorithmState.AS_USER_STOPED;
		 
		// there should be at least 2 chromosomes in population
		if( numberOfChromosomes < 2 )
			numberOfChromosomes = 2;

		// and algorithm should track at least on of best chromosomes
		if( trackBest < 1 )
			trackBest = 1;

		if( replaceByGeneration < 1 )
			this.replaceByGeneration = 1;
		else if( replaceByGeneration > numberOfChromosomes - trackBest )
			this.replaceByGeneration = numberOfChromosomes - trackBest;

		// reserve space for population
		chromosomes.setSize( numberOfChromosomes );
		bestFlags.setSize( numberOfChromosomes );

		// reserve space for best chromosome group
		bestChromosomes.setSize( trackBest );

		// clear population
		for( int i = (int)chromosomes.size() - 1; i >= 0; --i )
		{
			chromosomes.set(i, null);
			bestFlags.set(i, false);
		}
	}

	
	public void Start()
	{
		if(prototype == null)
			return;

		synchronized(this) 
		{
		  // do not run already running algorithm
		  if( state == AlgorithmState.AS_RUNNING )
			return;

		  state = AlgorithmState.AS_RUNNING;
		}

		if(observer != null)
			// notify observer that execution of algorithm has changed it state
			observer.EvolutionStateChanged(state);

		// clear best chromosome group from previous execution
		ClearBest();

		// initialize new population with chromosomes randomly built using prototype
		for(int i = 0; i < chromosomes.size(); ++i)
		{
			// remove chromosome from previous execution
			// add new chromosome to population
			Schedule newSchedule = prototype.MakeNewFromPrototype();
			chromosomes.set(i, newSchedule);
			AddToBest(i);
		}

		currentGeneration = 0;

		while(true)
		{
			if(useMaxGen && this.GetCurrentGeneration() >= maxGenNum)
			{
				break;
			}
			
			Schedule best = null;
			synchronized(this)
			{
			  // user has stopped execution?
			  if(state != AlgorithmState.AS_RUNNING)
			  {
				break;
			  }

			  best = GetBestChromosome();

			  // algorithm has reached criteria?
			  if(best.GetFitness() >= 1)
			  {
				state = AlgorithmState.AS_CRITERIA_STOPPED;
				break;
			  }
			}
			
			// produce offspring
			Vector<Schedule> offspring = new Vector<Schedule>();
			offspring.setSize(replaceByGeneration);
			for(int j = 0; j < replaceByGeneration; j++)
			{
				// selects parent randomly
				int index = (new Random().nextInt() % chromosomes.size() + chromosomes.size()) % chromosomes.size();
				Schedule p1 = chromosomes.get(index);
				Schedule p2 = chromosomes.get(index);

				offspring.set(j, p1.Crossover(p2));
				offspring.get(j).Mutation();
				observer.NewBestChromosome(offspring.get(j));
			}

			// replace chromosomes of current operation with offspring
			for(int j = 0; j < replaceByGeneration; j++)
			{
				int ci;
				do
				{
					// select chromosome for replacement randomly
					ci = (new Random().nextInt() % chromosomes.size() + chromosomes.size()) % chromosomes.size();

					// protect best chromosomes from replacement
				} while(IsInBest(ci));

				// replace chromosomes
				chromosomes.set(ci, offspring.get(j));

				// try to add new chromosomes in best chromosome group
				AddToBest(ci);
			}

			// algorithm has found new best chromosome
			if(best != GetBestChromosome() && observer != null)
			{
				// notify observer
				observer.NewBestChromosome(GetBestChromosome());
			}
				
			currentGeneration++;
		}

		if(observer != null)
		{
		    // notify observer that execution of algorithm has changed it state
			observer.NewBestChromosome(GetBestChromosome());
			observer.EvolutionStateChanged(state);
		}
	}

	// Stops execution of algoruthm
	public void Stop()
	{
		synchronized(this)
		{
		  // algorithm has found new best chromosome
		  if( state == AlgorithmState.AS_RUNNING)
		    state = AlgorithmState.AS_USER_STOPED;  
		}
	}

	// Returns pointer to best chromosomes in population
	Schedule GetBestChromosome()
	{
		return chromosomes.get(bestChromosomes.get(0));
	}

	// Tries to add chromosomes in best chromosome group
	void AddToBest(int chromosomeIndex)
	{
		// don't add if new chromosome hasn't fitness big enough for best chromosome group
		// or it is already in the group?
		if( ( currentBestSize == (int)bestChromosomes.size() && 
			chromosomes.get(bestChromosomes.get(currentBestSize - 1)).GetFitness() >= 
			chromosomes.get(chromosomeIndex).GetFitness()) || bestFlags.get(chromosomeIndex))
			return;

		// find place for new chromosome
		int i = currentBestSize;
		for( ; i > 0; i-- )
		{
			// group is not full?
			if( i < (int)bestChromosomes.size() )
			{
				// position of new chromosomes is found?
				if( chromosomes.get(bestChromosomes.get(i - 1)).GetFitness() > 
					chromosomes.get(chromosomeIndex).GetFitness() )
					break;

				// move chromosomes to make room for new
				bestChromosomes.set(i, bestChromosomes.get(i - 1));
			}
			else
				// group is full remove worst chromosomes in the group
				bestFlags.set(bestChromosomes.get(i - 1), false);
		}

		// store chromosome in best chromosome group
		bestChromosomes.set(i, chromosomeIndex);
		bestFlags.set(chromosomeIndex, true);

		// increase current size if it has not reached the limit yet
		if( currentBestSize < (int)bestChromosomes.size() )
			currentBestSize++;
	}

	// Returns TRUE if chromosome belongs to best chromosome group
	boolean IsInBest(int chromosomeIndex)
	{
		return bestFlags.get(chromosomeIndex);
	}

	// Clears best chromosome group
	void ClearBest()
	{
		for(int i = (int)bestFlags.size() - 1; i >= 0; --i)
			bestFlags.set(i, false);

		currentBestSize = 0;
	}
	
	// Returns current generation
	public int GetCurrentGeneration() { return currentGeneration; }

	// Returns pointe to algorithm's observer
	public ScheduleObserver GetObserver() { return observer; }
}
