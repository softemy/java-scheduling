package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Core.Configuration;
import Core.CourseClass;
import Core.Room;
import Core.Schedule;
import Core.StudentsGroup;

class CalendarPane extends JPanel {
	private static final long serialVersionUID = 7402805787294465276L;
	
	private static final int ROOM_CELL_WIDTH = 116;
	private static final int ROOM_CELL_HEIGHT = 66;

	private static final int ROOM_COLUMN_NUMBER = Schedule.DAYS_NUM + 1;
	private static final int ROOM_ROW_NUMBER = Schedule.DAY_HOURS + 1;
	private static final String[] DAYS = {"MON", "TUE", "WED", "THU", "FRI"};
	

	private int selectedRoomId;
	
	/**
	 * Background image
	 */
	private Image backgroundImg;
	private Image blue, grey, light;
	/** 
	 * Default constructor will construct this panel and add all mouse listener needed.
	 */
	private Schedule _schedule;
	
	/** IsdrawedBackground*/
	public CalendarPane() {				
		backgroundImg = new ImageIcon("icon\\backgrounds.png").getImage();
		blue = new ImageIcon("icon\\blue.png").getImage();
		grey = new ImageIcon("icon\\grey.png").getImage();
		light = new ImageIcon("icon\\light.png").getImage();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	    paintInt(g);
	}
	
	/**
	 * Update the panel and repaint it.
	 */
	public void update(Graphics g) {
		g.dispose();
		paintComponent(g);
	}
	
	/**
	 * Return width of this panel. Measured in pixels.
	 */
	public int getWidth() {
		return 1500;
	}
	
	/**
	 * Return height of this panel. Measured in pixels.
	 */
	public int getHeight() {
		return 881;
	}
	
	/**
	 * Resize this panel when a rectangle is dragged
	 */
	public void resizePanel() {
		Dimension newSize = new Dimension(this.getWidth(), this.getHeight());
		this.setSize(newSize);
		this.setPreferredSize(newSize);
	}		
	
	/**
	 * Draw background for this panel
	 */
	public void drawBackground(Graphics g) {
		for(int i = 0; i < this.getWidth();) {
			for(int j = 0; j < this.getHeight();) {
				g.drawImage(backgroundImg, i, j, this);
				j += 30;
			}
			i += 30;
		}
	}
	
	public void SetSchedule(Schedule schedule) 
	{
		this._schedule = schedule;
		
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				repaint();	
			}}
		); 
	}
	public void paintRoom(Graphics g, int roomId, int offsetX, int offsetY) {
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.BOLD, 18));
		for(int i = 0; i < ROOM_ROW_NUMBER; i++) {
			for(int j = 0; j < ROOM_COLUMN_NUMBER; j++) {
				if(i == 0 || j == 0) {
					g.drawImage(blue, offsetX + j * ROOM_CELL_WIDTH, offsetY + i * ROOM_CELL_HEIGHT, this);
					if(i == 0 && j > 0) {
						g.drawString(DAYS[j-1], offsetX + j * ROOM_CELL_WIDTH + 30, 33);
					}
					else if(j == 0 && i > 0) {
						String str = String.format("%d - %d", 9 + i - 1, 9 + i);
						g.drawString(str, offsetX + 22, offsetY+i*66 + 43);
					}
				}
				else if(i % 2 != 0 ){
					g.drawImage(grey, offsetX + j * ROOM_CELL_WIDTH, offsetY + i * ROOM_CELL_HEIGHT, this);
				}
				else {
					g.drawImage(light, offsetX + j * ROOM_CELL_WIDTH, offsetY + i * ROOM_CELL_HEIGHT, this);
				}
			}
		}
		Room room = Configuration.GetInstance().GetRoomById(roomId);
		g.drawString("Room: " + room.GetName().substring(1), offsetX + 10, offsetY + 20);
		String str = String.format("Lab: %c", room.IsLab() ? 'Y' : 'N');
		g.drawString(str, offsetX + 10, offsetY+40);
		str = String.format( "Seats: %d", room.GetNumberOfSeats());
		g.drawString(str, offsetX + 10, offsetY + 60);
		
		if(_schedule != null) {
			Font tableTextFont = new Font("Arial", Font.PLAIN, 10);
	        Font criteriaFont = new Font("Arial", Font.PLAIN, 12);
    		HashMap<CourseClass, Integer> classes = _schedule.GetClasses();
    		int ci = 0;
    		int nr = Configuration.GetInstance().GetNumberOfRooms();
    		Set<Map.Entry<CourseClass, Integer>> kvpSet = classes.entrySet();
    	    for (Iterator<Map.Entry<CourseClass, Integer>> it = kvpSet.iterator(); it.hasNext(); ci += 5)
    		{
    			Map.Entry<CourseClass, Integer> kvp = it.next();
    			CourseClass c = kvp.getKey();
    			int p = kvp.getValue();
    			
    			int t = p % ( nr * Schedule.DAY_HOURS );
    			int d = p / ( nr * Schedule.DAY_HOURS ) + 1;
    			int r = t / Schedule.DAY_HOURS;
    			if(r != roomId) continue;
    			t = t % Schedule.DAY_HOURS + 1;

    			Rectangle rect = new Rectangle( 
    				offsetX + d * ROOM_CELL_WIDTH,
    				offsetY + t * ROOM_CELL_HEIGHT,
    				ROOM_CELL_WIDTH,
    				ROOM_CELL_HEIGHT * c.GetDuration());
    			g.setColor(Color.BLACK);
    			g.drawRect(rect.x, rect.y, rect.width, rect.height);

    		    str = String.format("%s\n%s\n/", c.GetCourse().GetName(), c.GetLecturer().GetName());

    			for(Iterator<StudentsGroup> it2 = c.GetGroups().iterator(); 
    				it2.hasNext();)
    			{
    				str += it2.next().GetName();
    				str += "/";
    			}

    			if(c.IsLabRequired())
    				str += "\nLab";

    			rect.y += 5;
    			rect.height -= 5;
    			rect.x += 5;
    			rect.width -= 5;

    			g.setFont(tableTextFont);
    			
    			int lineWidth = 12;
    			int lineNum = str.length() / lineWidth;
    			if(str.length() % lineWidth != 0)
    			{
    				lineNum++;
    			}
    		
    			for(int i = 0; i < lineNum; i++)
    			{
    			  if((i + 1) * lineWidth > str.length())
    			  {
    			    g.drawString(str.substring(i * lineWidth), rect.x, rect.y + i * 15 + 10);
    			  }
    			  else
    			  {
    				g.drawString(str.substring(i * lineWidth, (i+1) * lineWidth), rect.x, rect.y + i * 15 + 10);
    			  }
    			}

    			g.setFont(criteriaFont);
  			
    			g.setColor(_schedule.GetCriteria().get(ci + 0) != null && _schedule.GetCriteria().get(ci + 0) ? new Color( 49, 147, 120 ) : new Color( 206, 0, 0 ));
    			g.drawString("R", rect.x, rect.y + rect.height - 10);
    			
    			g.setColor(_schedule.GetCriteria().get(ci + 1) != null && _schedule.GetCriteria().get(ci + 1) ? new Color( 49, 147, 120 ) : new Color( 206, 0, 0 ));
    			g.drawString("S", rect.x + 10, rect.y + rect.height - 10);
    			
    			g.setColor(_schedule.GetCriteria().get(ci + 2) != null && _schedule.GetCriteria().get(ci + 2) ? new Color( 49, 147, 120 ) : new Color( 206, 0, 0 ));
    			g.drawString("L", rect.x + 20, rect.y + rect.height - 10);
    			
    			g.setColor(_schedule.GetCriteria().get(ci + 3) != null && _schedule.GetCriteria().get(ci + 3) ? new Color( 49, 147, 120 ) : new Color( 206, 0, 0 ));
    			g.drawString("P", rect.x + 30, rect.y + rect.height - 10);
    			
    			g.setColor(_schedule.GetCriteria().get(ci + 4) != null && _schedule.GetCriteria().get(ci + 4) ? new Color( 49, 147, 120 ) : new Color( 206, 0, 0 ));
    			g.drawString("G", rect.x + 40, rect.y + rect.height - 10);

    			g.setColor(new Color( 0, 0, 0 ));
    		}
    	    g.setColor(new Color(255, 0, 0 ));
    		int i = 0;
    		for(Iterator<List<CourseClass>> it3 = _schedule.GetSlots().iterator();
    			it3.hasNext(); ++i )
    		{
    			List<CourseClass> clsList = it3.next();
    			if(clsList != null  && clsList.size() > 1)
    			{
    				int t = i % ( nr * Schedule.DAY_HOURS );
    				int d = i / ( nr * Schedule.DAY_HOURS ) + 1;
    				int r = t / Schedule.DAY_HOURS;
    				if(r != roomId) continue;
    				t = t % Schedule.DAY_HOURS + 1;
    				Rectangle rect = new Rectangle( 
    					offsetX + d * ROOM_CELL_WIDTH,
    					offsetY + t * ROOM_CELL_HEIGHT,
    					ROOM_CELL_WIDTH,
    					ROOM_CELL_HEIGHT);

    				g.drawRect(rect.x, rect.y, rect.width, rect.height);
    			}
    		}
		}
	}
	
	public void setSelectedRoom(int id) {
		selectedRoomId = id;
		repaint();
	}
    public void paintInt(Graphics g)
    {  
    	drawBackground(g);
    	int nr = Configuration.GetInstance().GetNumberOfRooms();
    	if(nr > 0) {
    		paintRoom(g, selectedRoomId, 0, 0);
    	}
    }
	
}
