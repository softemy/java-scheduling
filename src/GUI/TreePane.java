

package GUI;

import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import Core.Configuration;
import Core.Room;

class TreePane extends JTabbedPane {
	private static final long serialVersionUID = 1L;

	private JScrollPane classTreeScrollPane;
	private DefaultMutableTreeNode classRoot;
	private DefaultTreeCellRenderer classRenderer;	
	DefaultTreeModel fileModel;
	DefaultTreeModel classModel;
	DefaultTreeModel intModel;
	private JTree classTree;
	private CalendarPane drawPane;

	public TreePane(CalendarPane drawPane) {
		initRoot();

		classModel = new DefaultTreeModel(classRoot);
		classTree = new JTree(classModel);
		classTree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		classTree.addTreeSelectionListener(new RoomTreeSelectionListener());
		classRenderer = new DefaultTreeCellRenderer();
		ImageIcon classIcon = new ImageIcon("icon\\class_obj.gif");
		classRenderer.setClosedIcon(classIcon);
		classRenderer.setOpenIcon(classIcon);
		classRenderer.setLeafIcon(new ImageIcon("icon\\members.gif"));
		classTree.setCellRenderer(classRenderer);
		classTree.setShowsRootHandles(true);
		classTreeScrollPane = new JScrollPane(classTree);
		this.addTab("Room", new ImageIcon("icon\\class_hi.gif"),
				classTreeScrollPane, "Rooms imported");
		this.drawPane = drawPane;
	}

	/**
	 * Initialize all the roots.
	 */
	public void initRoot() {
		classRoot = new DefaultMutableTreeNode("Rooms");
	}

	public void addClassNode(Room room) {
		DefaultMutableTreeNode classNode = new DefaultMutableTreeNode(room);
		classRoot.add(classNode);
	}

	public DefaultMutableTreeNode getClassRoot() {
		return classRoot;
	}

	/**
	 * Refresh all tree when load a new config file
	 */
	public void refreshTree() {
		classRoot.removeAllChildren();
		HashMap<Integer, Room> rooms = Configuration.GetInstance().GetRooms();
		for(Room room: rooms.values()) {
			addClassNode(room);
		}
		classModel.reload();
	}

	public void updateClassTree() {
		Configuration.GetInstance().GetNumberOfRooms();
		classRoot.removeAllChildren();

		classModel.reload();
	}
	
	private class RoomTreeSelectionListener implements TreeSelectionListener {

		@Override
		public void valueChanged(TreeSelectionEvent arg0) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) classTree.getLastSelectedPathComponent();
			if(node == null)
				return ;
			Object roomNode = node.getUserObject();
			if(node.isLeaf() && !classRoot.isLeaf()) {
				Room room = (Room) roomNode;
				drawPane.setSelectedRoom(room.GetId());
				
			}
			
		}
		
	}

}