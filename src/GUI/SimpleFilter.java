
package GUI;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * SimpleFilter class will provide a file selection filter with 
 * the specified file extension and description.
 * A SimpleFilter object will be used in open or save dialog to filter 
 * needed file type to be view.
 */
class SimpleFilter extends FileFilter {
	private String extension;
	private String description;
	
	/**
	 * Construct a new SimpleFilter object.
	 * @param ext	Extension of file type will be filter.
	 * @param des	Description of file type will be provide for user. 
	 */
	public SimpleFilter(String ext, String des) {
		extension = ext;
		description = des;
	}
	
	/**
	 * Whether the given file is accepted by this filter.
	 */
	public boolean accept(File file) {
		if(file == null)
			return false;
		if(file.isDirectory())
			return true;
		if(extension == "*")
			return true;
		
		String fileName = file.getName().toLowerCase();
		return fileName.endsWith("." + extension);		
	}
	
	/**
	 * The description of this filter. For example: "Image files (*.jpg)"
	 */
	public String getDescription() {
		return description;
	}
}